#[cfg(test)]
mod tests {
  use crate::read_input;
  use crate::depth_increases;
  use crate::window_sum;

  #[test]
  fn part1() {
    let depths = read_input("input.txt".into()).unwrap();
    let answer = depth_increases(depths.into_iter());
    assert_eq!(answer, Some(1521));
  }

  #[test]
  fn part2() {
    let depths = read_input("input.txt".into()).unwrap();
    let answer = depth_increases(window_sum(depths.into_iter()));
    assert_eq!(answer, Some(1543));
  }
}

use std::fs::File;
use std::io::{BufRead, Error};
use std::str::FromStr;
use std::iter::Iterator;

pub struct WindowSum<'a> {
  first: Option<u32>,
  second: Option<u32>,
  third: Option<u32>,
  inner: Box<dyn Iterator<Item=u32> + 'a>
}

impl <'a> Iterator for WindowSum<'a> {
  type Item = u32;

  fn next(&mut self) -> Option<u32> {
    let sum = self.first? + self.second? + self.third?;
    self.first = self.second;
    self.second = self.third;
    self.third = self.inner.next();
    Some(sum)
  }
}

pub fn window_sum<'a, T: Iterator<Item=u32> + 'a>(iterator: T) -> WindowSum<'a> {
  let mut iterator = iterator;

  WindowSum{
    first: iterator.next(),
    second: iterator.next(),
    third: iterator.next(),
    inner: Box::new(iterator)
  }
}

pub fn depth_increases<T: Iterator<Item=u32>>(readings: T) -> Option<i32> {
  let mut readings = readings;
  let initial_depth = readings.nth(0)?;
  let (_, answer) = readings.fold((initial_depth, 0), |state, current| {
    let (previous, count) = state;
    (current, count + (if current > previous {1} else {0}))
  });
  return Some(answer);
}

pub fn read_input(file: String) -> Result<Vec<u32>, Error> {
  Ok(std::io::BufReader::new(File::open(file)?)
    .lines()
    .filter_map(|io| io.ok())
    .filter_map(|line| u32::from_str(&line).ok() )
    .collect())
}
